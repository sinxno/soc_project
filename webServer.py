from waitress import serve
from flask import Flask, send_file, request, redirect

app = Flask(__name__)


@app.route('/image')#Serves the Manually taken image
def SendImage():
    return send_file('image.jpg',mimetype='image/jpg')

@app.route('/MD')#Serves the motion detected image
def SendMDImage():
    return send_file('motionDetectedImage.jpg',mimetype='image/jpg')

@app.route('/')#Redirects to Node red dashboard
def NodeRed():
    return redirect('http://10.0.0.15:1880/ui/#/0')

serve(app,host='0.0.0.0', port=5000)#Starts the Flask server
