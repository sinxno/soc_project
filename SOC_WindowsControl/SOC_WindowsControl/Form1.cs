﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Drawing;

namespace SOC_WindowsControl
{
    public partial class Form1 : Form
    {
        private Thread workerThread;
        public String recievedUDPString;
        String currentCommand = "$DS"; //Initial value of the command
        bool canDrive = false; //The value of the physical switch on the robot



        public Form1()
        {
            InitializeComponent();
        }
                


        private void BuildCsv(String cmd)
        {
            ///Builds a CSV based on wanted command - No live change of speed

            //Combines the command and PWM value
            String command = cmd + "," + sliderSpeed.Value;

            //Stores the command
            currentCommand = cmd;

            //Updates the textbox tbCommand
            tbCommand.Text = command;

            //Calls the SendUDP
            SendUdp(command);
        }



        private void BuildCsv(int speed)
        {
            ///Builds a CSV based on live speed change
            
            //Combined the current command and the PWM value
            String command = currentCommand + "," + speed;


            //Updates the textbox tbCommand
            tbCommand.Text = command;

            //Calls the SendUDP
            SendUdp(command);
        }



        private void SendUdp(String message)
        {
            ///Send the command as UDP package
            

            byte[] data = new byte[1024];
            //sets the raspberry pi as the target
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("10.0.0.15"), 9050);

            //Sets up the UDP protocol
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //Adds a newline
            message = message + "\n";

            //Converts the message from ASCII to bytes
            data = Encoding.ASCII.GetBytes(message);

            //Sends the UDP message
            server.SendTo(data, data.Length, SocketFlags.None, ipep);
        }

        

        private void SliderSpeed_Scroll(object sender, EventArgs e)
        {
            ///Handles the speed slider
            
            //Displays the speed value in the sliderSpeed text box
            curSpeedValue.Text = sliderSpeed.Value.ToString() + "%";

            //If the live speed change checkbox is checked
            if (CbSpeedChange.Checked)
                //Build the csv when the slider changes
                BuildCsv(sliderSpeed.Value);
        }



        private void BtForward_Click(object sender, EventArgs e)
        {
            ///send a Drive forward command
            BuildCsv("$DF");
        }



        private void BtStop_Click(object sender, EventArgs e)
        {
            ///send a Stop command
            BuildCsv("$DS");
        }



        private void BtLeft_Click(object sender, EventArgs e)
        {
            ///send a Turn left command
            BuildCsv("$DL");
        }



        private void BtFastLeft_Click(object sender, EventArgs e)
        {
            ///send a Turn left fast command
            BuildCsv("$DLF");
        }



        private void BtRight_Click(object sender, EventArgs e)
        {
            ///send a Turn right command
            BuildCsv("$DR");
        }



        private void BtFastRight_Click(object sender, EventArgs e)
        {
            ///send a Turn right fast command
            BuildCsv("$DRF");
        }



        private void BtReverse_Click(object sender, EventArgs e)
        {
            ///send a Drive backward command
            BuildCsv("$DB");
        }



        private void Btn_TakePicture_Click(object sender, EventArgs e)
        {
            String command = "$TP";
            tbCommand.Text = command;
            SendUdp(command);
            timer1.Enabled = true;
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            ///Sets up a recieve UDP thread
                      
            workerThread = new Thread(this.DoRecieveUDP);
            workerThread.IsBackground = true;
            workerThread.Start();
        }

        

        public void UdpDataRecieved(object sender, EventArgs e)
        {
            ///Called when a UDP message is recieved

            //Updates the tbRecieved textbox with the udp message
            tbRecieved.Text = recievedUDPString;

            //If the message is $PT or $MD, start timer 1
            if((String.Compare(recievedUDPString,"$PT") == 0) || (String.Compare(recievedUDPString, "$MD") == 0))
                timer1.Enabled = true;
            else
            {
                String[] s = recievedUDPString.Split(',');
                if (String.Compare(s[0], "$LID") == 0) //If light value message
                {
                    lb_luxLabel.Text = "Current Light Value = " + s[1] + " Lux"; // Update the light value text
                }
                else if (String.Compare(s[0], "$SS") == 0)//If Drive activated/deactivated
                {
                    canDrive = Convert.ToBoolean(Convert.ToInt16(s[1]));//Set the can drive bool
                    panel1.Invalidate();//Refresh the graphics panel
                }
            }

            
        }



        public void DoRecieveUDP()
        {
            ///Recieve UDP thread

            //Set up the UDP listener
            UdpClient sock = new UdpClient(9050);
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 0);

            while (true)
            {
                try
                {
                    //recieve udp message
                    byte[] data = sock.Receive(ref iep);
                    //convert Bytes to ascii string
                    recievedUDPString = Encoding.ASCII.GetString(data, 0, data.Length);
                    // call the function UdpDataRecieved
                    this.Invoke(new EventHandler(this.UdpDataRecieved));
                }
                catch(Exception e) { }

            }
            sock.Close();
        }


        
        private void Timer1_Tick(object sender, EventArgs e)
        {
            ///Loads pictures from webserver to image boxes
            ///Needs to be done with an timer, because images is not ready on the
            ///server imideately

            pictureBox1.Load("http://10.0.0.15:5000/image");
            pictureBox2.Load("http://10.0.0.15:5000/MD");
            timer1.Enabled = false;
        }



        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            //based on https://www.c-sharpcorner.com/article/how-to-draw-shapes-inside-panel-control-in-winforms-using-visual-studio-2017/

            Graphics g = panel1.CreateGraphics(); //The graphics object
            Rectangle rect = new Rectangle(0, 0, 20, 20); //The size of the elipse
            SolidBrush sb = new SolidBrush(Color.Red); //If not can drive, Color = Red

            if (canDrive)
                sb.Color = Color.Green; //If can drive, Color = Green

            g.FillEllipse(sb, rect); //Create the elipse
        }
    }
}
