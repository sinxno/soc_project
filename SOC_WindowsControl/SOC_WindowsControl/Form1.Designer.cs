﻿namespace SOC_WindowsControl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btForward = new System.Windows.Forms.Button();
            this.btLeft = new System.Windows.Forms.Button();
            this.btRight = new System.Windows.Forms.Button();
            this.btReverse = new System.Windows.Forms.Button();
            this.btFastRight = new System.Windows.Forms.Button();
            this.btFastLeft = new System.Windows.Forms.Button();
            this.btStop = new System.Windows.Forms.Button();
            this.sliderSpeed = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.curSpeedValue = new System.Windows.Forms.Label();
            this.tbCommand = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbRecieved = new System.Windows.Forms.TextBox();
            this.CbSpeedChange = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Btn_TakePicture = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lb_luxLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sliderSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btForward
            // 
            this.btForward.Location = new System.Drawing.Point(173, 17);
            this.btForward.Name = "btForward";
            this.btForward.Size = new System.Drawing.Size(75, 23);
            this.btForward.TabIndex = 0;
            this.btForward.Text = "Forward";
            this.btForward.UseVisualStyleBackColor = true;
            this.btForward.Click += new System.EventHandler(this.BtForward_Click);
            // 
            // btLeft
            // 
            this.btLeft.Location = new System.Drawing.Point(99, 46);
            this.btLeft.Name = "btLeft";
            this.btLeft.Size = new System.Drawing.Size(75, 23);
            this.btLeft.TabIndex = 1;
            this.btLeft.Text = "Left";
            this.btLeft.UseVisualStyleBackColor = true;
            this.btLeft.Click += new System.EventHandler(this.BtLeft_Click);
            // 
            // btRight
            // 
            this.btRight.Location = new System.Drawing.Point(248, 46);
            this.btRight.Name = "btRight";
            this.btRight.Size = new System.Drawing.Size(75, 23);
            this.btRight.TabIndex = 2;
            this.btRight.Text = "Right";
            this.btRight.UseVisualStyleBackColor = true;
            this.btRight.Click += new System.EventHandler(this.BtRight_Click);
            // 
            // btReverse
            // 
            this.btReverse.Location = new System.Drawing.Point(173, 75);
            this.btReverse.Name = "btReverse";
            this.btReverse.Size = new System.Drawing.Size(75, 23);
            this.btReverse.TabIndex = 3;
            this.btReverse.Text = "Reverse";
            this.btReverse.UseVisualStyleBackColor = true;
            this.btReverse.Click += new System.EventHandler(this.BtReverse_Click);
            // 
            // btFastRight
            // 
            this.btFastRight.Location = new System.Drawing.Point(329, 46);
            this.btFastRight.Name = "btFastRight";
            this.btFastRight.Size = new System.Drawing.Size(75, 23);
            this.btFastRight.TabIndex = 4;
            this.btFastRight.Text = "Fast Right";
            this.btFastRight.UseVisualStyleBackColor = true;
            this.btFastRight.Click += new System.EventHandler(this.BtFastRight_Click);
            // 
            // btFastLeft
            // 
            this.btFastLeft.Location = new System.Drawing.Point(18, 46);
            this.btFastLeft.Name = "btFastLeft";
            this.btFastLeft.Size = new System.Drawing.Size(75, 23);
            this.btFastLeft.TabIndex = 5;
            this.btFastLeft.Text = "Fast Left";
            this.btFastLeft.UseVisualStyleBackColor = true;
            this.btFastLeft.Click += new System.EventHandler(this.BtFastLeft_Click);
            // 
            // btStop
            // 
            this.btStop.Location = new System.Drawing.Point(173, 46);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(75, 23);
            this.btStop.TabIndex = 6;
            this.btStop.Text = "Stop";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.BtStop_Click);
            // 
            // sliderSpeed
            // 
            this.sliderSpeed.Location = new System.Drawing.Point(99, 104);
            this.sliderSpeed.Maximum = 100;
            this.sliderSpeed.Name = "sliderSpeed";
            this.sliderSpeed.Size = new System.Drawing.Size(224, 45);
            this.sliderSpeed.TabIndex = 8;
            this.sliderSpeed.TickFrequency = 10;
            this.sliderSpeed.Scroll += new System.EventHandler(this.SliderSpeed_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Speed:";
            // 
            // curSpeedValue
            // 
            this.curSpeedValue.AutoSize = true;
            this.curSpeedValue.Location = new System.Drawing.Point(329, 107);
            this.curSpeedValue.Name = "curSpeedValue";
            this.curSpeedValue.Size = new System.Drawing.Size(21, 13);
            this.curSpeedValue.TabIndex = 10;
            this.curSpeedValue.Text = "0%";
            // 
            // tbCommand
            // 
            this.tbCommand.Location = new System.Drawing.Point(173, 155);
            this.tbCommand.Name = "tbCommand";
            this.tbCommand.Size = new System.Drawing.Size(224, 20);
            this.tbCommand.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Last sent message:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Last recieved message:";
            // 
            // tbRecieved
            // 
            this.tbRecieved.Location = new System.Drawing.Point(173, 185);
            this.tbRecieved.Name = "tbRecieved";
            this.tbRecieved.Size = new System.Drawing.Size(224, 20);
            this.tbRecieved.TabIndex = 11;
            // 
            // CbSpeedChange
            // 
            this.CbSpeedChange.AutoSize = true;
            this.CbSpeedChange.Location = new System.Drawing.Point(382, 107);
            this.CbSpeedChange.Name = "CbSpeedChange";
            this.CbSpeedChange.Size = new System.Drawing.Size(117, 17);
            this.CbSpeedChange.TabIndex = 13;
            this.CbSpeedChange.Text = "Live speed change";
            this.CbSpeedChange.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "http://10.0.0.15:5000/image";
            this.pictureBox1.Location = new System.Drawing.Point(55, 307);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 240);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // Btn_TakePicture
            // 
            this.Btn_TakePicture.Location = new System.Drawing.Point(55, 278);
            this.Btn_TakePicture.Name = "Btn_TakePicture";
            this.Btn_TakePicture.Size = new System.Drawing.Size(320, 23);
            this.Btn_TakePicture.TabIndex = 15;
            this.Btn_TakePicture.Text = "Take Picture";
            this.Btn_TakePicture.UseVisualStyleBackColor = true;
            this.Btn_TakePicture.Click += new System.EventHandler(this.Btn_TakePicture_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.ImageLocation = "http://10.0.0.15:5000/MD";
            this.pictureBox2.Location = new System.Drawing.Point(442, 307);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(320, 240);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(442, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Motion Detection Picture:";
            // 
            // lb_luxLabel
            // 
            this.lb_luxLabel.AutoSize = true;
            this.lb_luxLabel.Location = new System.Drawing.Point(52, 236);
            this.lb_luxLabel.Name = "lb_luxLabel";
            this.lb_luxLabel.Size = new System.Drawing.Size(186, 13);
            this.lb_luxLabel.TabIndex = 18;
            this.lb_luxLabel.Text = "Current Light Value is not recieved yet";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(498, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(31, 31);
            this.panel1.TabIndex = 19;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(416, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Drive Activated:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 633);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lb_luxLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.Btn_TakePicture);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.CbSpeedChange);
            this.Controls.Add(this.tbRecieved);
            this.Controls.Add(this.tbCommand);
            this.Controls.Add(this.curSpeedValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sliderSpeed);
            this.Controls.Add(this.btStop);
            this.Controls.Add(this.btFastLeft);
            this.Controls.Add(this.btFastRight);
            this.Controls.Add(this.btReverse);
            this.Controls.Add(this.btRight);
            this.Controls.Add(this.btLeft);
            this.Controls.Add(this.btForward);
            this.Name = "Form1";
            this.Text = "Robot Control Center";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sliderSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btForward;
        private System.Windows.Forms.Button btLeft;
        private System.Windows.Forms.Button btRight;
        private System.Windows.Forms.Button btReverse;
        private System.Windows.Forms.Button btFastRight;
        private System.Windows.Forms.Button btFastLeft;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.TrackBar sliderSpeed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label curSpeedValue;
        private System.Windows.Forms.TextBox tbCommand;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbRecieved;
        private System.Windows.Forms.CheckBox CbSpeedChange;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Btn_TakePicture;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lb_luxLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
    }
}

