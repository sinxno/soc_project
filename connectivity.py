# -*- coding: utf-8 -*-

#---------------------------------------------------------------------#
# This is the main communication script of the whole EN-SOC project.  #
# All communications between the systems goes through this script.    #
# It uses UDP to communicate internally.                              #
# The following communication lines exists:                           #
#       Nucleo card - Serial communication                            #
#       Windows PC runnding .NET application                          #
#       Node red dashboard                                            #
#       Wii nunchuck                                                  #
#       USB webcam                                                    #
#       GPIO                                                          #
#       Bluetooth                                                     #
#---------------------------------------------------------------------#


import serial #Serial port API http://pyserial.sourceforge.net/pyserial_api.html
import socket
import time
from threading import Thread
import smbus
import cv2
import numpy as np
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(16,gpio.OUT)
gpio.setup(20,gpio.OUT)
gpio.setup(21,gpio.IN)
gpio.output(16,gpio.HIGH)
gpio.output(20,gpio.LOW)

switchState = gpio.input(21)
oldSwitchState = gpio.input(21)

prev_joy_x = None
prev_joy_y = None
prev_btnC = None

cyclesCounter = 0                           #Count cycles
moving = 0                                  #Is the robot moving?

#Video Capture
cap = cv2.VideoCapture(0)

#History, Treshold, DetectShadows
fgbg = cv2.createBackgroundSubtractorMOG2(50,200,True)

#Keeps track of current frame
framecount = 0

port = "/dev/ttyACM0"                       #Serial port for the nucleo board
port_ble = "/dev/ttyACM1"                   #Serial port for the ble device

UDP_IP = "10.0.0.6"                         #IP adress to the windows PC
UDP_IP_Local = "127.0.0.1"                  #Localhost
UDP_PORT = 9050                             #UDP port

sock = socket.socket(socket.AF_INET,        # Internet protocol
                     socket.SOCK_DGRAM)     # User Datagram (UDP)
sock.bind(("0.0.0.0", UDP_PORT))            # Listen on all adapters

SerialIONucleo = serial.Serial(port,9600)   # setup the nucleo serial port and baudrate
SerialIONucleo.flushInput()                 # Remove old input's

SerialIOble = serial.Serial(port_ble,9600)  # setup the ble serial port and baudrate
SerialIOble.flushInput()                    # Remove old input's



#VIDEO CAPTURE THREAD
def Video(capture, frameCount):             # Based on: https://github.com/AhadCove/motion-Detection/
    global moving
    print("Video thread started")
    
    while True:
        prevFrameCount = 0
        
        while (moving == 0):
            #Return Value and the current frame
            ret,frame = capture.read()

            #Check if current frame actually exist
            if not ret:
                break
            #Increase frame count
            frameCount += 1

            #Resize the frame
            resizedFrame = cv2.resize(frame, (0, 0), fx=0.50, fy=0.50)

            #get the foreground mask
            fgmask = fgbg.apply(resizedFrame)

            #Count all the non zero pixels
            count = np.count_nonzero(fgmask)

            #If framecount is over 1 and pixels changed is over 1000 motion is detected
            if (frameCount > 1 and count > 1000):
                #If there is over 10 frames since last picture was changed
                if (frameCount - prevFrameCount) > 10:
                    #Write the image to disc
                    cv2.imwrite('motionDetectedImage.jpg',frame)
                    #Send a motion detected message via UDP
                    sock.sendto("$MD", (UDP_IP, UDP_PORT))
                    #Set previous frame count to current frame count
                    prevFrameCount = frameCount



#BLUETOOTH THREAD                    
#Recieves serial data from the ble device, and sends it as an internal udp package
def bleSerial(ble):
    print("bleSerial thread Started")
    
    while True:
        #If there is an bluetooth serial message waiting
        if (ble.inWaiting() > 0):
            inputLine = ble.readline().strip()  # read a '\n' terminated line()
            print "BLE received message:", inputLine
            
            inputLine=inputLine+'\n'            #Adds an newline character to the message
            sock.sendto(inputLine, (UDP_IP_Local, UDP_PORT))#Send the message as a local UDP package
        


#RECIEVE UDP THREAD
#Recievs an udp message and sends it to the nucleo serial port
def recvUDP(sock,SerialIONucleo):
    print("recvUDP thread Started")
    
    while True:
        data, addr = sock.recvfrom(1280) # Max recieve size is 1280 bytes
        print "UDP received message:", data.strip()
        if (data.strip() == "$TP"):#If the command is take picture
            takePicture(True, cap)#Take the picture
        else:
            SerialIONucleo.write(data)#Else, forward the command to the nucleo serial
            sock.sendto(data, ("127.0.0.1", 9060))#Makes the message available to NodeRed



#SEND UDP THREAD
#Recieves an serial message from the nucleo card, and sends it as an udp package to the windows PC
def sendUDP(sock, SerialIONucleo):
    global moving
    global framecount
    print("sendUDP thread Started")
    
    while True:
        #If there is an serial message waitng
        if (SerialIONucleo.inWaiting() > 0):
            inputLine = SerialIONucleo.readline().strip()  # read a '\n' terminated line()
            csv = inputLine.split(",")#Split the string into an string array

            #If the command is "Driving"
            if csv[0] == '$DRV':
                moving = int(csv[1]) #Set the moving variable to the second part of the command

                if moving == 1:
                    framecount = 0#Reset the framecount
                    
            # Send the csv string as a UDP message
            sock.sendto(inputLine, (UDP_IP, UDP_PORT))#Windows PC
            sock.sendto(inputLine, ("127.0.0.1", 9060))#Node-Red
            print "UDP message sendt:", inputLine



#Takes a picture with the usb camera
def takePicture(do,capture):
    if(do):
        for i in xrange(4):
            capture.grab() #Empty the buffer to only grab the latest image
        b,img = capture.read()#Stores the data from the camera

        if b:
            cv2.imwrite('image.jpg',img)#Saves the picture to file
            sock.sendto("$PT", (UDP_IP, UDP_PORT)) #Sends a confirmation message to the windows PC
        else:
            print("Camera not working")
            sock.sendto("Camera not working", (UDP_IP, UDP_PORT))




#Y axis on the wii nunchuck has changed
def Joy_y_Command(joy_y):
    joy_y = float(joy_y)#Casts the value to a float 
    #If the y axis is down
    if (joy_y <128):
        pwm = int(((127-joy_y)/127)*100)#Convert the y value to a 0-100 value
        message = "$DB,"+ str(pwm) + "\n"#Build the message
    else:#y axis is up
        pwm = int(((joy_y - 127)/127)*100)
        message = "$DF,"+ str(pwm) + "\n"
    sock.sendto(message, (UDP_IP_Local, UDP_PORT))#Send the message as a local UDP package


    
#X axis on the wii nunchuck has changed. Works exactly as Joy_y_Command
def Joy_x_Command(joy_x):
    joy_x = float(joy_x)
    if (joy_x <128):
        pwm = int(((127-joy_x)/127)*100)
        message = "$DL,"+ str(pwm) + "\n"
    else:
        pwm = int(((joy_x - 127)/127)*100)
        message = "$DR,"+ str(pwm) + "\n"
    sock.sendto(message, (UDP_IP_Local, UDP_PORT))



#called when joystick changes to neutral position
def Stop_Command():
    sock.sendto("$DS\n", (UDP_IP_Local, UDP_PORT))#Send the message as a local UDP package



#Start Recieve UDP thread - Forwards UDP messages to Nucleo card
t = Thread(target=recvUDP,args=(sock,SerialIONucleo,))
t.daemon=True # Stop thread when program ends
t.start()

#Start Send UDP thread - Forward Serial messages from nucleo as UDP messages to Windows PC
t2 = Thread(target=sendUDP,args=(sock,SerialIONucleo,))
t2.daemon=True # Stop thread when program ends
t2.start()

#Start BLE Serial thread - Forward the Bluetooth messages as local UDP messages
t3 = Thread(target=bleSerial,args=(SerialIOble,))
t3.daemon=True # Stop thread when program ends
t3.start()

#Start the Video thread - This thread takes care of the motion detection
t4 = Thread(target=Video, args=(cap, framecount,))
t4.daemon = True
t4.start()

print "UDP target IP:",   UDP_IP
print "UDP target port:", UDP_PORT

bus = smbus.SMBus(1) #I2C bus
address = 0x52 #Adress to the Wii nunchuck

#Write startup messages to the Nunchuck
bus.write_byte_data(address, 0x40, 0x00)
bus.write_byte_data(address, 0xF0, 0x55)
bus.write_byte_data(address, 0xFB, 0x00)
time.sleep(0.1)

#Send state of Drive activation Switch
sock.sendto("$SS,"+str(switchState)+"\r\n", (UDP_IP, UDP_PORT))#Windows PC
sock.sendto("$SS,"+str(switchState)+"\r\n", ("127.0.0.1", 9060))#Node-Red
sock.sendto("$SS,"+str(switchState)+"\r\n", (UDP_IP_Local, UDP_PORT))#Nucleo


#MAIN LOOP
while True:
    
    switchState = gpio.input(21)#Update switch state

    if oldSwitchState is not switchState: #if switch state has changed, send switch state
        sock.sendto("$SS,"+str(switchState)+"\r\n", (UDP_IP, UDP_PORT))#Windows PC
        sock.sendto("$SS,"+str(switchState)+"\r\n", ("127.0.0.1", 9060))#Node-Red
        sock.sendto("$SS,"+str(switchState)+"\r\n", (UDP_IP_Local, UDP_PORT))#Nucleo
        
    oldSwitchState = switchState #Store switch state
    
    try:
        bus.write_byte(address,0x00)#Get messages from Nunchuck
        time.sleep(0.1)

        #Read all the bytes from the Nunchuck
        data0 = bus.read_byte(address)
        data1 = bus.read_byte(address)
        data2 = bus.read_byte(address)
        data3 = bus.read_byte(address)
        data4 = bus.read_byte(address)
        data5 = bus.read_byte(address)
        data = [data0, data1, data2, data3, data4, data5]
        
        joy_x = data[0]#X-value from the nunchuck
        joy_y = data[1]#Y-value from the nunchuck

        #Set up dead zone for the nunchuck joystick
        if(joy_x > 120 and joy_x < 140):
            joy_x = 128
        if(joy_y > 120 and joy_y < 140):
            joy_y = 128

        
        btnC = (data[5]&0x02)>>1 #C button on the nunchuck is the second bit of 5th byte from the nunchuck
        btnC = not btnC #Inverted logic on the C button

        if switchState:
            #If the joystick has changed to the neutral position
            if (((joy_x is not prev_joy_x) or (joy_y is not prev_joy_y)) and (joy_x is 128) and (joy_y is 128)):
                Stop_Command() #Stop the robot
            else:
                if (joy_x is not prev_joy_x):#If the x axis has changed
                    Joy_x_Command(joy_x)#Send a turn command to the robot - This has priority

                elif (joy_y is not prev_joy_y):#If the y axis has changed
                    Joy_y_Command(joy_y)#Send a drive forward/backward to the robot - This is only activated if th joustick is not to the left or right

        #If C button is changed
        if (btnC is not prev_btnC):
            takePicture(btnC, cap)#Run the take picture function. Also sending if C button is true or false

        #Sets the current values of the joystick as the previous values
        prev_joy_x = joy_x
        prev_joy_y = joy_y
        prev_btnC = btnC

        #Gets the current light value. Do this every 50 cycles
        if cyclesCounter == 0:
            sock.sendto("$DTL\n", (UDP_IP_Local, UDP_PORT))
            cyclesCounter = 50

        cyclesCounter -= 1

    except IOError as e:
        print e
