#based on https://learn.adafruit.com/circuitpython-nrf52840/button-press

from adafruit_ble.uart import UARTServer
from adafruit_bluefruit_connect.packet import Packet
from adafruit_bluefruit_connect.button_packet import ButtonPacket
import time
import board
from digitalio import DigitalInOut, Direction
 
uart_server = UARTServer()

led=DigitalInOut(board.LED1)
led.direction=Direction.OUTPUT
led.value = 1
ledR = DigitalInOut(board.LED2_R)
ledR.direction=Direction.OUTPUT
ledG = DigitalInOut(board.LED2_G)
ledG.direction=Direction.OUTPUT
ledB = DigitalInOut(board.LED2_B)
ledB.direction=Direction.OUTPUT
ledG.value = 1
ledB.value = 1
ledR.value = 0


while True:
    
    uart_server.start_advertising()
    while not uart_server.connected:
        led.value = 1
        ledR.value = 0
        ledG.value = 1
        ledB.value = 1
 
    # Now we're connected
 
    while uart_server.connected:
        led.value = 0
        ledR.value = 1
        ledG.value = 0
        if uart_server.in_waiting:
            packet = Packet.from_stream(uart_server)
            if isinstance(packet, ButtonPacket):
                if packet.pressed:
                    if packet.button == ButtonPacket.UP:
                        # The UP button was pressed. Drive forwards. Blue Light LED2
                        print("$DF,50")
                        ledG.value = 1
                        ledB.value = 0
                    elif packet.button == ButtonPacket.DOWN:
                        # The DOWN button was pressed. Drive backwards. Blue Light LED2
                        print("$DB,50")
                        ledG.value = 1
                        ledB.value = 0
                    elif packet.button == ButtonPacket.LEFT:
                        # The LEFT button was pressed. Drive left. Blue Light LED2
                        print("$DL,50")
                        ledG.value = 1
                        ledB.value = 0
                    elif packet.button == ButtonPacket.RIGHT:
                        # The RIGHT button was pressed. Drive right. Blue Light LED2
                        print("$DR,50")
                        ledG.value = 1
                        ledB.value = 0
                    elif packet.button == ButtonPacket.BUTTON_1:
                        # The 1 Button was pressed. Take picture. Blue and Green Light LED2
                        print("$TP")
                        ledG.value = 0
                        ledB.value = 0
                else:
                    #No button pressed. Stop. Green light LED2
                    print("$DS")
                    ledG.value = 0
                    ledB.value = 1
			
                
                        
 
    # If we got here, we lost the connection. Go up to the top and start
    # advertising again and waiting for a connection.
