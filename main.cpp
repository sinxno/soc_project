/*-----------------------------------------------------------------------------|
| Code to controll the Adafruit Motor Shield V1                                |
|                                                                              |
| The card needs a quick mod to work with NUCLEO cards:                        |
| http://www.emcu.eu/how-to-use-the-adafruit-motor-shield-on-stm32-nucleo/     |
|                                                                              |
| Simple program to test the mod:                                              |
| https://os.mbed.com/users/emcu/code/AdafruitCTRLmot/                         |
|                                                                              |
| Left Motor must be connected to M1 and right motor to M2                     |
|                                                                              |
|                                                                              |
| Recieves serial commands to controll motor M1 and M2:                        |
| $DF,xxx = Drive forward with PWM 0-100                                       |
| $DB,xxx = Drive Backwards with PWM 0-100                                     |
| $DL,xxx = Turn left using 1 motor with PWM 0-100                             |
| $DLF,xxx = Turn left using 2 motors with PWM 0-100                           |
| $DR,xxx = Turn right using 1 motor with PWM 0-100                            |
| $DRF,xxx = Turn right using 2 motors with PWM 0-100                          |
| $DS = Stop                                                                   |
| $DTL = Detect Light Intensity                                                |
| $SS,x = State of switch                                                      |
------------------------------------------------------------------------------*/

#include "mbed.h"

Serial pc(USBTX,USBRX);         //Sets up serial

DigitalOut clkPin(PB_5);        //Clocks the shift register
DigitalOut latchPin(PA_6);      //Latches the shift register
DigitalOut Enable(PA_8);        //Enables the shift register
DigitalOut data(PA_9);          //data input of the shift register

PwmOut pwmM1(PA_7);             //pwm pin for Motor 1
PwmOut pwmM2(PB_3);             //pwm pin for Motor 2

DigitalOut Vcc(A1);             //+3.3V
AnalogIn aIn(A2);               //LDR Diode
DigitalOut Gnd(A0);             //0V

int pwm;
bool canDrive = false;

//Defines constants for the bit patterns on the shift register
#define FORWARD 0
#define BACKWARD 1
#define LEFT 2
#define RIGHT 3
#define LEFTFAST 4
#define RIGHTFAST 5
#define STOP 6

void DriveCommand(int cmd, int pwm);
void BitShiftCommand(int row);
void MeasureLight();

const int motorBits[7][8]= {        //Bit patterns to control the shift registers on the motor shield
    //   X  X  X M2B M1A M1B M2A X     //X are connected to other outputs on the motor shield, and are not used in this application
    { 0, 0, 0, 0,  1,  0,  1, 0},   // FORWARD
    { 0, 0, 0, 1,  0,  1,  0, 0},   // BACKWARD
    { 0, 0, 0, 0,  0,  0,  1, 0},   // ROTATE LEFT SLOW
    { 0, 0, 0, 0,  1,  0,  0, 0},   // ROTATE RIGHT SLOW
    { 0, 0, 0, 0,  0,  1,  1, 0},   // ROTATE LEFT FAST
    { 0, 0, 0, 1,  1,  0,  0, 0},   // ROTATE RIGHT FAST
    { 0, 0, 0, 0,  0,  0,  0, 0}    // STOP
};



int main()
{
    printf("Ready to accept commands\r\n");
    pwmM1.period_ms(20);            //50 Hz PWM
    pwmM2.period_ms(20);
    Enable = 0;                     //Make sure the motors starts disabled 
    Vcc = 1; Gnd = 0;

    while(1) {
        char buffer[80];
        scanf("%[^\r\n]s", buffer);             //Waits on Serial input

        getchar();

        //vars used to split csv string
        char* tok1;                             //holds the first string element
        char* tok2;                             //holds the second string element

        tok1 = strtok(buffer, ",");             //get the first element in string before the "," (This is the command)
        tok2 = strtok(NULL, ",");               //get the second element in string before the "," (this is the PWM value as char)

        pwm = atoi(tok2);                   //converts the PWM char value to int
        if (pwm < 0)                            //keeps the PWM between 0 and 100
            pwm=0;
        else if (pwm>100)
            pwm=100;

        if (strcmp(tok1, "$SS") == 0)
        {
            if(pwm == 1)
                canDrive = true;
            else
                canDrive = false;
        }

        if(canDrive)
        {
            //Starts the DriveCommand functions depending on the CSV command that is input
            if (strcmp(tok1, "$DF") == 0)
                DriveCommand(FORWARD, pwm);
            else if (strcmp(tok1, "$DB") == 0)
                DriveCommand(BACKWARD, pwm);
            else if (strcmp(tok1, "$DL") == 0)
                DriveCommand(LEFT, pwm);
            else if (strcmp(tok1, "$DR") == 0)
                DriveCommand(RIGHT, pwm);
            else if (strcmp(tok1, "$DS") == 0)
                DriveCommand(STOP, 0);
            else if (strcmp(tok1, "$DLF") == 0)
                DriveCommand(LEFTFAST, pwm);
            else if (strcmp(tok1, "$DRF") == 0)
                DriveCommand(RIGHTFAST, pwm);
            else if (strcmp(tok1, "$DTL") == 0)
                MeasureLight();
        }

        buffer[0] = '\0';                       //empty the buffer
    }//End while(True)
}//End main



void DriveCommand(int cmd, int pwm) //pwm are 0-100
{
    float dutyC = (float)pwm/100.0f; //Converts the PWM to range 0.0 - 1.0 that is expected on the pwm pin
    BitShiftCommand(cmd); //Sends out the bit pattern.
    pwmM1.write(dutyC);//Sets the pwm duty cycle
    pwmM2.write(dutyC);
    
    if(cmd == STOP || dutyC == 0)
        printf("$DRV,0\r\n");
    else
        printf("$DRV,1\r\n");
}//End DriveCommand



void BitShiftCommand(int row)
{
    latchPin=0; //Makes sure the output pins of the shift register is not set untill the bit pattern is loaded

    for(int i=0; i<=7; i++) {
        data=motorBits[row][i];//Sets one bit on the datapin
        wait_ms(1);
        clkPin=1;               //moves all bits on 'place' on the shift register
        wait_ms(1);
        clkPin=0;
        wait_ms(1);
    }

    latchPin=1;  //Sets the output pins on the shift register
    wait_ms(5);
    latchPin=0; //Makes sure the bit pattern is not changed unintended
    wait_ms(1);
    Enable = 1; //activates the output
    wait_ms(1);
    Enable = 0;
    wait_ms(1);
}//End bitShiftCommand



void MeasureLight()
{
    //Calculate the lux value
    if(aIn>0.0f)
    {
        double lux =(500.0f/10000.0f)*((3.3f - aIn)/aIn);
    
        printf("$LID,%.2f\r\n", lux);
    }
}
